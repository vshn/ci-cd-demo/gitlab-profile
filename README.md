This group describes how a CI/CD process for small teams could look like.

- An application consisting of multiple services
- Internal staging environment
- Own production environment
- `N` customer environments, also managed by us